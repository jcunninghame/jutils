package data;

import math.Percentage;
import org.apache.commons.lang3.RandomStringUtils;

import java.math.BigDecimal;
import java.util.concurrent.ThreadLocalRandom;

public class RandomData {

    public static String randomAlphabetic(){
        return RandomStringUtils.randomAlphanumeric(10);
    }

    public static String randomAlphabetic(int numChars){
        return RandomStringUtils.randomAlphanumeric(numChars);
    }

    public static String randomNumeric(int numInts){
        return RandomStringUtils.randomNumeric(numInts);
    }

    public static String randomAlphaNumeric(){
        return RandomStringUtils.randomAlphanumeric(10);
    }

    public static String randomAlphaNumeric(int numInts){
        return RandomStringUtils.randomAlphanumeric(numInts);
    }

    public static int randomInt() {
        return ThreadLocalRandom.current().nextInt(0, 10);
    }

    public static int randomInt(int max) {
        return ThreadLocalRandom.current().nextInt(0, max + 1);
    }

    public static int randomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    public static BigDecimal randomBigDecimal(BigDecimal max) {
        return new BigDecimal(Math.random()).divide(max, BigDecimal.ROUND_DOWN);
    }

    public static BigDecimal randomBigDecimal(BigDecimal min, BigDecimal max) {
        BigDecimal randomBigDecimal = min.add(new BigDecimal(Math.random()).multiply(max.subtract(min)));
        return randomBigDecimal.setScale(2, BigDecimal.ROUND_DOWN);
    }

    public static boolean randomBoolean() {
        return ThreadLocalRandom.current().nextBoolean();
    }

    public static <T extends Enum<?>> T randomEnum(Class<T> clazz){
        int x = ThreadLocalRandom.current().nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    public static Percentage randomPercentage() {
        return randomPercentage(Percentage.HundredPercent);
    }

    public static Percentage randomPercentage(Percentage max) {
        return new Percentage(new BigDecimal(Math.random()).multiply(max.toBigDecimal()));
    }

    public static Percentage randomPercentage(Percentage min, Percentage max) {
        BigDecimal randomBigDecimal = min.toBigDecimal().add(new BigDecimal(Math.random()).multiply(max.toBigDecimal().subtract(min.toBigDecimal())));
        return new Percentage(randomBigDecimal.setScale(2, BigDecimal.ROUND_DOWN));
    }
}
