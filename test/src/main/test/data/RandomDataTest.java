package data;

import math.Percentage;
import org.junit.Test;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

public class RandomDataTest {

    @Test
    public void randomPercentageShouldAlwaysReturnAValueBetweenZeroAndOneHundred() {
        Percentage randomPercentage = RandomData.randomPercentage();

        assertThat(randomPercentage, greaterThanOrEqualTo(Percentage.ZeroPercent));
        assertThat(randomPercentage, lessThanOrEqualTo(Percentage.HundredPercent));
    }

    @Test
    public void randomPercentageShouldNotExceedMaximum() {
        Percentage max = new Percentage("0.05");

        assertThat(RandomData.randomPercentage(max), lessThanOrEqualTo(max));
    }

    @Test
    public void randomPercentageShouldNotExceedMaximumOrBeLessThanMinimum() {
        Percentage max = new Percentage("10.45");
        Percentage min = new Percentage("10.23");
        Percentage randomPercentage = RandomData.randomPercentage(min, max);

        assertThat(randomPercentage, greaterThanOrEqualTo(min));
        assertThat(randomPercentage, lessThanOrEqualTo(max));
    }
}