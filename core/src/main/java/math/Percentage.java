package math;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.math.BigDecimal;

public class Percentage implements Comparable<Percentage> {

    public static Percentage ZeroPercent = new Percentage("0");
    public static Percentage HundredPercent = new Percentage("100");

    private BigDecimal value;

    public Percentage(String s) {
        value = new BigDecimal(s);
    }

    public Percentage(BigDecimal b) {
        value = b;
    }

    public Percentage add(Percentage percentage) {
        BigDecimal newValue = value.add(percentage.toBigDecimal());
        return new Percentage(newValue);
    }

    public Percentage add(Percentage ... percentages) {
        BigDecimal total = value;
        for (Percentage percentage : percentages) {
            total = total.add(percentage.toBigDecimal());
        }
        return new Percentage(total);
    }

    public Percentage subtract(Percentage percentage) {
        BigDecimal newValue = value.subtract(percentage.toBigDecimal());
        return new Percentage(newValue);
    }

    public Percentage subtract(Percentage ... percentages) {
        BigDecimal total = value;
        for (Percentage percentage : percentages) {
            total = total.subtract(percentage.toBigDecimal());
        }
        return new Percentage(value);
    }

    public BigDecimal toBigDecimal() {
        return value;
    }

    public boolean isEqualTo(Percentage percentage) {
        return value.compareTo(percentage.toBigDecimal()) == 0;
    }

    public boolean isNotEqualTo(Percentage percentage) {
        return value.compareTo(percentage.toBigDecimal()) != 0;
    }

    public boolean isGreaterThan(Percentage percentage) {
        return value.compareTo(percentage.toBigDecimal()) == 1;
    }

    public boolean isGreaterThanOrEqualTo(Percentage percentage) {
        return value.compareTo(percentage.toBigDecimal()) != -1;
    }

    public boolean isLessThan(Percentage percentage) {
        return value.compareTo(percentage.toBigDecimal()) == -1;
    }

    public boolean isLessThanOrEqualTo(Percentage percentage) {
        return value.compareTo(percentage.toBigDecimal()) != 1;
    }

    @Override
    public String toString() {
        return value.toPlainString() + "%";
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public int compareTo(Percentage p) {
        return this.toBigDecimal().compareTo(p.toBigDecimal());
    }
}
