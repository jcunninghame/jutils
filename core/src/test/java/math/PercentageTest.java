package math;

import org.junit.Test;

import static math.PercentageMatcher.matchesPercent;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class PercentageTest {

    private final Percentage CONTROL = new Percentage("47.327");
    private final Percentage SMALLERVALUE = new Percentage("13.02");
    private final Percentage SLIGHTLYSMALLERVALUE = new Percentage("47.326");
    private final Percentage EQUALVALUE = new Percentage("47.327");
    private final Percentage SLIGHTLYGREATERVALUE = new Percentage("47.328");
    private final Percentage GREATERVALUE = new Percentage("84.246");

    @Test
    public void shouldCorrectlyEvaluateIsEqualTo() throws Exception {
        assertTrue(CONTROL.isEqualTo(EQUALVALUE));
        assertFalse(CONTROL.isEqualTo(GREATERVALUE));
    }

    @Test
    public void shouldCorrectlyEvaluateIsNotEqualTo() throws Exception {
        assertFalse(CONTROL.isNotEqualTo(EQUALVALUE));
        assertTrue(CONTROL.isNotEqualTo(GREATERVALUE));
        assertTrue(CONTROL.isNotEqualTo(SMALLERVALUE));
    }

    @Test
    public void shouldCorrectlyEvaluateIsGreaterThan() throws Exception {
        assertTrue(CONTROL.isGreaterThan(SMALLERVALUE));
        assertTrue(CONTROL.isGreaterThan(SLIGHTLYSMALLERVALUE));
        assertTrue(CONTROL.isGreaterThan(SMALLERVALUE));
        assertFalse(CONTROL.isGreaterThan(EQUALVALUE));
        assertFalse(CONTROL.isGreaterThan(SLIGHTLYGREATERVALUE));
        assertFalse(CONTROL.isGreaterThan(GREATERVALUE));
    }

    @Test
    public void shouldCorrectlyEvaluateIsGreaterThanOrEqualTo() throws Exception {
        assertTrue(CONTROL.isGreaterThanOrEqualTo(SMALLERVALUE));
        assertTrue(CONTROL.isGreaterThanOrEqualTo(SLIGHTLYSMALLERVALUE));
        assertTrue(CONTROL.isGreaterThanOrEqualTo(SMALLERVALUE));
        assertTrue(CONTROL.isGreaterThanOrEqualTo(EQUALVALUE));
        assertFalse(CONTROL.isGreaterThanOrEqualTo(SLIGHTLYGREATERVALUE));
        assertFalse(CONTROL.isGreaterThanOrEqualTo(GREATERVALUE));
    }

    @Test
    public void shouldCorrectlyEvaluateIsLessThan() throws Exception {
        assertFalse(CONTROL.isLessThan(SMALLERVALUE));
        assertFalse(CONTROL.isLessThan(SLIGHTLYSMALLERVALUE));
        assertFalse(CONTROL.isLessThan(SMALLERVALUE));
        assertFalse(CONTROL.isLessThan(EQUALVALUE));
        assertTrue(CONTROL.isLessThan(SLIGHTLYGREATERVALUE));
        assertTrue(CONTROL.isLessThan(GREATERVALUE));
    }

    @Test
    public void shouldCorrectlyEvaluateIsLessThanOrEqualTo() throws Exception {
        assertFalse(CONTROL.isLessThanOrEqualTo(SMALLERVALUE));
        assertFalse(CONTROL.isLessThanOrEqualTo(SLIGHTLYSMALLERVALUE));
        assertFalse(CONTROL.isLessThanOrEqualTo(SMALLERVALUE));
        assertTrue(CONTROL.isLessThanOrEqualTo(EQUALVALUE));
        assertTrue(CONTROL.isLessThanOrEqualTo(SLIGHTLYGREATERVALUE));
        assertTrue(CONTROL.isLessThanOrEqualTo(GREATERVALUE));
    }

    @Test
    public void shouldHaveCorrectAbsolutes() {
        assertThat(Percentage.ZeroPercent, is(new Percentage("0")));
        assertThat(Percentage.HundredPercent, is(new Percentage("100")));
    }

    @Test
    public void shouldCorrectlyAddTwoPercentages() {
        Percentage percentage1 = new Percentage("13.44");
        Percentage percentage2 = new Percentage("49.901");

        Percentage expected = new Percentage("63.341");
        Percentage additionTest = percentage1.add(percentage2);

        assertThat(additionTest, matchesPercent(expected));
    }

    @Test
    public void shouldCorrectlyAddMultiplePercentages() {
        Percentage percentage1 = new Percentage("10.21");
        Percentage percentage2 = new Percentage("14.278");
        Percentage percentage3 = new Percentage("17.199");
        Percentage percentage4 = new Percentage("7.01");

        Percentage expected = new Percentage("48.697");
        Percentage additionTest = percentage1.add(percentage2, percentage3, percentage4);

        assertThat(additionTest, matchesPercent(expected));
    }

    @Test
    public void shouldCorrectlySubtractPercentages() {
        Percentage percentage1 = new Percentage("63.341");
        Percentage percentage2 = new Percentage("49.901");

        Percentage expected = new Percentage("13.44");
        Percentage subtractionTest = percentage1.subtract(percentage2);

        assertThat(subtractionTest, matchesPercent(expected));
    }


}