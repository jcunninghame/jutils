package math;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

public class PercentageMatcher extends BaseMatcher<Percentage> {

    private final Percentage percentage;

    public static Matcher<? super Percentage> matchesPercent(final Percentage percentage) {
        return new PercentageMatcher(percentage);
    }

    public PercentageMatcher(Percentage percentage) {
        this.percentage = percentage;
    }

    public boolean matches(Object o) {
        return percentage.isEqualTo((Percentage) o);
    }

    public void describeTo(Description description) {
        description.appendText(percentage.toString());
    }
}
