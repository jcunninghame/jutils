package web;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class HttpExceptionTest {
    
    @Test
    public void shouldHaveTheCorrectDefaultMessage() {
        try {
            throw new ResourceNotFoundException();
        } catch (ResourceNotFoundException rnfe) {
            assertThat(rnfe.getMessage(), is("404: Resource not found"));
        }
    }

}