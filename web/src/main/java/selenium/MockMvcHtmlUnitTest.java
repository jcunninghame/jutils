package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.test.context.TestExecutionListeners;

import java.lang.annotation.*;

import static org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@TestExecutionListeners(
        listeners = MockMvcHtmlUnitTestExecutionListener.class,
        mergeMode = MERGE_WITH_DEFAULTS)
public @interface MockMvcHtmlUnitTest {

    String baseUrl() default "http://localhost:9000";
}
